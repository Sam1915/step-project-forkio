import gulp from 'gulp';

import gulpSass from 'gulp-sass';
import dartSass from 'sass';
import browsersync from 'browser-sync';

const sass = gulpSass(dartSass);

const buildCss = () => {
	return gulp
		.src('./src/scss/main.scss')
		.pipe(sass({ outputStyle: 'expanded' }))
		.pipe(gulp.dest('./dist/css'))
		.pipe(browsersync.stream());
}

const watchCss = () => {
	gulp.watch('./src/scss/**/*.scss', buildCss)
}
gulp.task('default', gulp.parallel(buildCss, watchCss));